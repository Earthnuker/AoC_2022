use std::cmp::Reverse;

use aoc_runner_derive::{aoc, aoc_generator};
#[aoc_generator(day1)]
pub fn input_generator(input: &str) -> Vec<usize> {
    input
        .split("\n\n")
        .map(|chunk| {
            chunk
                .lines()
                .map(|line| line.trim().parse::<usize>().unwrap())
                .sum()
        })
        .collect()
}

#[aoc(day1, part1)]
pub fn solve_part1(input: &[usize]) -> usize {
    input.iter().max().copied().unwrap_or(0)
}

#[aoc(day1, part2)]
pub fn solve_part2(input: &[usize]) -> usize {
    let mut scores: Vec<usize> = input.to_vec();
    scores.sort_by_key(|v| Reverse(*v));
    scores.iter().take(3).sum()
}
