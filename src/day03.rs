use aoc_runner_derive::{aoc, aoc_generator};
use std::collections::HashSet;
#[aoc_generator(day3)]
pub fn input_generator(input: &str) -> Vec<Vec<char>> {
    input.lines().map(|line| line.chars().collect()).collect()
}

#[aoc(day3, part1)]
pub fn solve_part1(input: &[Vec<char>]) -> usize {
    let mut total: usize = 0;
    for line in input {
        let (a, b) = line.split_at(line.len() / 2);
        let a: HashSet<&char> = a.iter().collect();
        let b: HashSet<&char> = b.iter().collect();
        total += a
            .intersection(&b)
            .map(|&&v| match v {
                'a'..='z' => 1 + ((v as u8) - b'a') as usize,
                'A'..='Z' => 27 + ((v as u8) - b'A') as usize,
                _ => unreachable!(),
            })
            .sum::<usize>();
    }
    total
}

#[aoc(day3, part2)]
pub fn solve_part2(input: &[Vec<char>]) -> usize {
    let mut total: usize = 0;
    for chunk in input.chunks_exact(3) {
        let chunk: HashSet<&char> = chunk
            .iter()
            .map(|v| v.iter().collect::<HashSet<&char>>())
            .reduce(|a, b| a.intersection(&b).copied().collect())
            .unwrap_or_default();
        total += chunk
            .iter()
            .map(|&&v| match v {
                'a'..='z' => 1 + ((v as u8) - b'a') as usize,
                'A'..='Z' => 27 + ((v as u8) - b'A') as usize,
                _ => unreachable!(),
            })
            .sum::<usize>();
    }
    total
}
